﻿using System;

namespace ToDoList.Repo.Interface
{
    public interface IToDo
    {
            int Id { get; set; }
            DateTime StartDate { get; set; }
            string Summary { get; set; }
            string Title { get; set; }        
    }
}
