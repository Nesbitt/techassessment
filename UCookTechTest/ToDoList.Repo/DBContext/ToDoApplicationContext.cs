﻿using Microsoft.EntityFrameworkCore;
using ToDoList.Repo.Model;
using ToDoList.Repo.ModelMap;

namespace ToDoList.Repo.DBContext
{

    public class ToDoApplicationContext : DbContext
    {

        public DbSet<ToDo> toDo { get; set; }
        public ToDoApplicationContext(DbContextOptions<ToDoApplicationContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            new ToDoMap(modelBuilder.Entity<ToDo>());
        }
    }
}

