﻿using System;
using ToDoList.Repo.Interface;
namespace ToDoList.Repo.Model
{
    public class ToDo : IToDo
    {
            public int Id { get; set; }
            public string Title { get; set; }
            public string Summary { get; set; }
            public DateTime StartDate { get; set; }
    }
}
