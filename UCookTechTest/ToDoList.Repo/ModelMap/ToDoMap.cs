﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ToDoList.Repo.Model;

namespace ToDoList.Repo.ModelMap
{
    public class ToDoMap
    {

        public ToDoMap(EntityTypeBuilder<ToDo> entityBuilder)
        {
            entityBuilder.HasKey(t => t.Id);
            entityBuilder.Property(t => t.StartDate).IsRequired();
        }

    }
}
