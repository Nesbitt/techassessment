﻿using FluentValidation;
using ToDoList.Domain.CommandResponse;
using ToDoList.Domain.Interface;
using ToDoList.Domain.ValidationExtension;

namespace ToDoList.Domain.Command
{

    public class AddToDoItemCommand : ICommand<AddToDoItemResponse>
    {

        public string Title { get; set; }

        public string Summary { get; set; }

        public string StartDate { get; set; }
    }

    public class AddToDoItemValidator : AbstractValidator<AddToDoItemCommand>
    {
        public AddToDoItemValidator()
        {
            RuleFor(el => el.Title).NotEmpty();
            RuleFor(el => el.StartDate).NotNull();
            RuleFor(el => el.StartDate).SetValidator(new MustBeAValidDate<AddToDoItemCommand>());
        }

    }
}
