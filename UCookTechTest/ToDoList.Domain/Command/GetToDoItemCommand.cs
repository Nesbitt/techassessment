﻿using FluentValidation;
using ToDoList.Domain.CommandResponse;
using ToDoList.Domain.Interface;

namespace ToDoList.Domain.Command
{
    public class GetToDoItemCommand : ICommand<GetToDoItemResponse>
    {
        public int Id { get; set; }
    }

    public class GetToDoItemValidator : AbstractValidator<GetToDoItemCommand>
    {
        public GetToDoItemValidator()
        {
            RuleFor(el => el.Id).NotEmpty();
        }
    }

}
