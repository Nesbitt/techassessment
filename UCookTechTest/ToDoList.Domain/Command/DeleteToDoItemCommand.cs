﻿using FluentValidation;
using ToDoList.Domain.CommandResponse;
using ToDoList.Domain.Interface;
namespace ToDoList.Domain.Command
{
    public class DeleteToDoItemCommand : ICommand<StandardResponse>
    {
        public int Id { get; set; }
    }

    public class DeleteToDoItemValidator : AbstractValidator<DeleteToDoItemCommand>
    {
        public DeleteToDoItemValidator()
        {
            RuleFor(el => el.Id).NotEmpty();
        }
    }


}

