﻿using System.Collections.Generic;
using ToDoList.Domain.CommandResponse;
using ToDoList.Domain.Interface;

namespace ToDoList.Domain.Command
{
    public class GetAllToDoItemsCommand : ICommand<GetAllToDoItemsResponse>
    {
        // nothing to be sent
    }

    
}
