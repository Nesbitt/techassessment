﻿using FluentValidation;
using System;
using ToDoList.Domain.CommandResponse;
using ToDoList.Domain.Interface;
using ToDoList.Domain.ValidationExtension;

namespace ToDoList.Domain.Command
{
    public class UpdateToDoItemCommand : ICommand<UpdateToDoItemResponse>
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string StartDate { get; set; }
    }

    public class UpdateToDoItemValidator : AbstractValidator<UpdateToDoItemCommand>
    {
        public UpdateToDoItemValidator()
        {
            RuleFor(el => el.Id).GreaterThan(0);
            RuleFor(el => el.Title).NotEmpty();
            RuleFor(el => el.StartDate).NotNull();
            RuleFor(el => el.StartDate).SetValidator(new MustBeAValidDate<AddToDoItemCommand>());
        }

    }
}

