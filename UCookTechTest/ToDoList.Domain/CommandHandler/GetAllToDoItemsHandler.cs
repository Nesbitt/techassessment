﻿using System;
using System.Threading.Tasks;
using ToDoList.Domain.Command;
using ToDoList.Domain.CommandResponse;
using ToDoList.Domain.Interface;

namespace ToDoList.Domain.CommandHandler
{
    public class GetAllToDoItemsHandler : ValidatorHandler<GetAllToDoItemsCommand, GetAllToDoItemsResponse>
    {
        private IToDoService _toDoService;

        public GetAllToDoItemsHandler(IToDoService toDoService)
        {
            _toDoService = toDoService;
        }
        public override Task<GetAllToDoItemsResponse> HandleRequest(GetAllToDoItemsCommand request)
        {
            try
            {
                return Task.FromResult(new GetAllToDoItemsResponse
                {
                    ToDoItems = _toDoService.GetAllToDos(),
                    Message = "Success",
                    Successful = true
                });
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}


