﻿using System;
using System.Threading.Tasks;
using ToDoList.Domain.Command;
using ToDoList.Domain.CommandResponse;
using ToDoList.Domain.Interface;
using ToDoList.Domain.ObjectMapper;

namespace ToDoList.Domain.CommandHandler
{
    public class AddToDoItemHandler : ValidatorHandler<AddToDoItemCommand, AddToDoItemResponse>
    {

        private IToDoService _toDoService;

        public AddToDoItemHandler(IToDoService toDoService)
        {
            _toDoService = toDoService;
        }

        public override Task<AddToDoItemResponse> HandleRequest(AddToDoItemCommand request)
        {
            try
            {
                var toDoItem = ToDoMapper.MapToDoItemCommandToToDoItem(request);
                var returnedToDoItem = _toDoService.AddToDo(toDoItem);
                return Task.FromResult(new AddToDoItemResponse { Id= returnedToDoItem.Id,  Successful = true, Message = "To do item successfully added." });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
