﻿using System;
using System.Threading.Tasks;
using ToDoList.Domain.Command;
using ToDoList.Domain.CommandResponse;
using ToDoList.Domain.Interface;
using ToDoList.Domain.ObjectMapper;

namespace ToDoList.Domain.CommandHandler
{
    public class UpDateToDoItemHandler : ValidatorHandler<UpdateToDoItemCommand, UpdateToDoItemResponse>
    {
       
        private IToDoService _toDoService;

        public UpDateToDoItemHandler(IToDoService toDoService)
        {
            _toDoService = toDoService;
        }
        public override Task<UpdateToDoItemResponse> HandleRequest(UpdateToDoItemCommand request)
        {
            try
            {
                var toDoItem = ToDoMapper.MapUpdateToDoItemCommandToToDoItem(request);
                _toDoService.UpdateToDo(toDoItem);
                return Task.FromResult(new UpdateToDoItemResponse { Id=toDoItem.Id, Successful = true, Message = "To do item successfully updated." });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
