﻿using System;
using System.Threading.Tasks;
using ToDoList.Domain.Command;
using ToDoList.Domain.CommandResponse;
using ToDoList.Domain.Interface;

namespace ToDoList.Domain.CommandHandler
{
    public class DeleteToDoItemHandler : ValidatorHandler<DeleteToDoItemCommand, StandardResponse>
    {
        private IToDoService _toDoService;

        public DeleteToDoItemHandler(IToDoService toDoService)
        {
            _toDoService = toDoService;
        }
        public override Task<StandardResponse> HandleRequest(DeleteToDoItemCommand request)
        {
            try
            {
                // find the todo item that we will remove
                var itemToDelete = _toDoService.GetToDo(request.Id);
                if (itemToDelete != null)
                {
                    _toDoService.DeleteToDo(itemToDelete);
                    return Task.FromResult(new StandardResponse { Successful = true, Message = "To do item successfully deleted." });
                }
                return Task.FromResult(new StandardResponse { Successful = false, Message = "To do item not deleted." });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
