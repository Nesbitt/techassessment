﻿using System;
using System.Threading.Tasks;
using ToDoList.Domain.Command;
using ToDoList.Domain.CommandResponse;
using ToDoList.Domain.Interface;
using ToDoList.Domain.ObjectMapper;

namespace ToDoList.Domain.CommandHandler
{
    public class GetToDoItemHandler : ValidatorHandler<GetToDoItemCommand, GetToDoItemResponse>
    {
        private IToDoService _toDoService;

        public GetToDoItemHandler(IToDoService toDoService)
        {
            _toDoService = toDoService;
        }

        public override Task<GetToDoItemResponse> HandleRequest(GetToDoItemCommand request)
        {
            try
            {
                // find task 
                var item = _toDoService.GetToDo(request.Id);
                if (item != null)
                {
                    var response = ToDoMapper.MapToDoItemToGetToDoItemResponse(item);
                    response.Successful = true;
                    response.Message = "Success";
                    return Task.FromResult(response);
                }
                return Task.FromResult(new GetToDoItemResponse { Successful = false, Message = "Unable to find task." });
               
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
