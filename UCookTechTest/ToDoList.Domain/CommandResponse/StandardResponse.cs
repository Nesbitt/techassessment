﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoList.Domain.CommandResponse
{
    public class StandardResponse 
    {
        public Boolean Successful { get; set; }
        public string Message { get; set; }

    }
}
