﻿namespace ToDoList.Domain.CommandResponse
{
    public class AddToDoItemResponse : StandardResponse
    {
        public int Id { get; set; }
    }
}
