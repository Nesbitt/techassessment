﻿using System;

namespace ToDoList.Domain.CommandResponse
{
    public class GetToDoItemResponse : StandardResponse
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public DateTime StartDate { get; set; }
    }
}
