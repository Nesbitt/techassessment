﻿using System.Collections.Generic;
using ToDoList.Repo.Model;

namespace ToDoList.Domain.CommandResponse
{
    public class GetAllToDoItemsResponse : StandardResponse
    {
        public List<ToDo> ToDoItems { get; set; }
    }
}
