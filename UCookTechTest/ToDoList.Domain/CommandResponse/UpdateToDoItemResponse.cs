﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoList.Domain.CommandResponse
{
    public class UpdateToDoItemResponse : StandardResponse
    {
        public int Id { get; set; }
    }
}
