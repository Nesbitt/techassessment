﻿using AutoMapper;
using System.Collections.Generic;
using ToDoList.Domain.Command;
using ToDoList.Domain.CommandResponse;
using ToDoList.Repo.Model;

namespace ToDoList.Domain.ObjectMapper
{
    public static class ToDoMapper
    {

        // this will only handle the basic object maps but will allow us to code the more complex ones
        static ToDoMapper()
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<AddToDoItemCommand, ToDo>();
                cfg.CreateMap<UpdateToDoItemCommand, ToDo>();
                cfg.CreateMap<GetToDoItemCommand, ToDo>();
                cfg.CreateMap<ToDo, GetToDoItemResponse>();
                cfg.CreateMap<ToDo, AddToDoItemCommand>();
                cfg.CreateMap<ToDo, UpdateToDoItemResponse>();
                cfg.CreateMap<List<ToDo>, List<GetToDoItemResponse>>();
            });
        }

        public static ToDo MapToDoItemCommandToToDoItem(AddToDoItemCommand request)
        {
            return Mapper.Map<AddToDoItemCommand, ToDo>(request);
        }

        public static ToDo MapUpdateToDoItemCommandToToDoItem(UpdateToDoItemCommand request)
        {
            return Mapper.Map<UpdateToDoItemCommand, ToDo>(request);
        }

        public static ToDo MapGetToDoItemCommandToDoItem(GetToDoItemCommand request)
        {
            return Mapper.Map<GetToDoItemCommand, ToDo>(request);
        }

        public static GetToDoItemResponse MapToDoItemToGetToDoItemResponse(ToDo item)
        {
            return Mapper.Map<ToDo, GetToDoItemResponse>(item);
        }

        public static AddToDoItemResponse MapToDoItemToAddToDoItemResponse(ToDo item)
        {
            return Mapper.Map<ToDo, AddToDoItemResponse>(item);
        }

        public static UpdateToDoItemResponse MapToDoItemToUpdateToDoItemResponse(ToDo item)
        {
            return Mapper.Map<ToDo, UpdateToDoItemResponse>(item);
        }

        public static List<GetToDoItemResponse> MapListToDoItemsToListGetToDoResponse(List<ToDo> itemsList)
        {
            return Mapper.Map<List<ToDo>, List<GetToDoItemResponse>>(itemsList);
        }
        
    }
}
