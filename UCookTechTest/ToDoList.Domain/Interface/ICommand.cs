﻿using MediatR;

namespace ToDoList.Domain.Interface
{
    public interface ICommand<T> : IRequest<T>
    {
    }
}
