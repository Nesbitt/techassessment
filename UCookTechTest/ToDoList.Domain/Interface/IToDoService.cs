﻿using System.Collections.Generic;
using ToDoList.Repo.Model;

namespace ToDoList.Domain.Interface
{
    public interface IToDoService
    {
        List<ToDo> GetAllToDos();
        ToDo AddToDo(ToDo toDo);
        ToDo GetToDo(int id);
        void UpdateToDo(ToDo toDo);
        void DeleteToDo(ToDo toDo);
    }
}
