﻿using FluentValidation.Validators;
using System;

namespace ToDoList.Domain.ValidationExtension
{
    public class MustBeAValidDate<T> : PropertyValidator
    {
        public MustBeAValidDate()
        : base("Property {PropertyName} must be a valid date!")
        {

        }
        protected override bool IsValid(PropertyValidatorContext context)
        {
          DateTime date;
            return DateTime.TryParse(context.PropertyValue.ToString(), out date);
        }

    }
}
