﻿using System.Collections.Generic;
using System.Linq;
using ToDoList.Domain.Interface;
using ToDoList.Repo.DBContext;
using ToDoList.Repo.Model;
namespace ToDoList.Domain.Service
{
    public class ToDoService : IToDoService
    {

        private ToDoApplicationContext _db;

        public ToDoService(ToDoApplicationContext db)
        {
            this._db = db;
        }

       
        public List<ToDo> GetAllToDos()
        {
            return _db.Set<ToDo>().ToList();
        }

        public ToDo AddToDo(ToDo toDo)
        {
            _db.Add(toDo);
            _db.SaveChanges();
            return toDo;
        }

        public ToDo GetToDo(int id)
        {
            return _db.toDo.Where(t => t.Id == id).FirstOrDefault();
        }

        public void UpdateToDo(ToDo todo)
        {
            _db.Update(todo);
            _db.SaveChanges();
        }

        public void DeleteToDo(ToDo todo)
        {
            _db.Remove(todo);
            _db.SaveChanges();
        }
    }
}
