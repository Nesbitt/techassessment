﻿using Common.Interfaces;
using FluentValidation;
using MediatR;
using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Handlers
{
    public abstract class ValidatorHandler<TRequest, TResponse>
    : IRequestHandler<TRequest, TResponse>
    where TRequest : ICommand<TResponse>
    {
        public abstract Task<TResponse> HandleRequest(TRequest request);

        private IValidator CreateValidatorForCommand(TRequest request)
        {
            var vt = typeof(AbstractValidator<>);
            var et = request.GetType();
            var evt = vt.MakeGenericType(et);

            var validatorType = Assembly.GetAssembly(et).GetTypes().FirstOrDefault(t => t.IsSubclassOf(evt)); ;
            if (validatorType == null)
                return null;

            var validatorInstance = (IValidator)Activator.CreateInstance(validatorType);
            return validatorInstance;
        }

        public TResponse Handle(TRequest request)
        {
            // this is obsolete but we need to make sure nothing is calling it
            throw new NotImplementedException();
        }

        public Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken = default(CancellationToken))
        {
            var validator = CreateValidatorForCommand(request);
            if (validator == null)
                return HandleRequest(request);

            var context = new ValidationContext(request);
            var failures = validator
                .Validate(context)
                .Errors
                .Where(f => f != null)
                .ToList();

            if (failures.Any())
                throw new ValidationException(failures);

            return HandleRequest(request);
        }
    }
}
