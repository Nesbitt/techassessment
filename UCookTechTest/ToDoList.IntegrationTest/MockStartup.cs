﻿using System.Threading.Tasks;

namespace ToDoList.IntegrationTest
{
    using FakeItEasy;
    using MediatR;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Respawn;
    using System;
    using System.IO;
    using ToDoList.Repo.DBContext;
    public class MockStartup
    {
        private static readonly Checkpoint _checkpoint;
        private static readonly IConfiguration _configuration;
        private static readonly IServiceScopeFactory _scopeFactory;

        static MockStartup()
        {
            // let's mock up an app start
            var host = A.Fake<IHostingEnvironment>();
            var configenv = A.Fake<IConfiguration>();
            A.CallTo(() => host.ContentRootPath).Returns(Directory.GetCurrentDirectory());
            var startup = new API.Startup(configenv);
            _configuration = startup.Configuration;
            var services = new ServiceCollection();          
            services.AddTransient<Domain.Interface.IToDoService, Domain.Service.ToDoService>();
            // use the local db, move this to the app json settings
            services.AddDbContext<ToDoApplicationContext>(options => options.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=UCookTechTest;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"));
            startup.ConfigureServices(services);
            var provider = services.BuildServiceProvider();
            _scopeFactory = provider.GetService<IServiceScopeFactory>();
            _checkpoint = new Checkpoint();
        }

        public static Task ResetCheckpoint() => _checkpoint.Reset("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=UCookTechTest;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");


        public static async Task ExecuteScopeAsync(Func<IServiceProvider, Task> action)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetService<ToDoApplicationContext>();
                try
                {
                    // we haven't implemented the transactions yet
                    //dbContext.BeginTransaction();
                    await action(scope.ServiceProvider);
                    // await dbContext.CommitTransactionAsync();
                }
                catch (Exception ex)
                {
                    //dbContext.RollbackTransaction();
                    throw;
                }
            }
        }

        public static async Task<T> ExecuteScopeAsync<T>(Func<IServiceProvider, Task<T>> action)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetService<ToDoApplicationContext>();

                try
                {
                    // we haven't implemented the transactions yet
                    //dbContext.BeginTransaction();
                    var result = await action(scope.ServiceProvider);
                    //await dbContext.CommitTransactionAsync();
                    return result;
                }
                catch (Exception ex)
                {
                    //dbContext.RollbackTransaction();
                    throw;
                }
            }
        }

        public static Task ExecuteDbContextAsync(Func<ToDoApplicationContext, Task> action)
        {
            return ExecuteScopeAsync(sp => action(sp.GetService<ToDoApplicationContext>()));
        }

        public static Task<T> ExecuteDbContextAsync<T>(Func<ToDoApplicationContext, Task<T>> action)
        {
            return ExecuteScopeAsync(sp => action(sp.GetService<ToDoApplicationContext>()));
        }

        public static Task Insert<T>(T entity) where T : class
        {
            return ExecuteDbContextAsync(db =>
            {
                    db.Add(entity);
                    return db.SaveChangesAsync();
            });
        }

        public static Task ClearDB<T>() where T : class
        {
            return ExecuteDbContextAsync(db =>
                {
                    db.Set<T>().RemoveRange(db.Set<T>());
                    return db.SaveChangesAsync();
                });
        }
                

        public static Task<T> Get<T>(int id)
            where T : class
        {
            return ExecuteDbContextAsync(db => db.Set<T>().FindAsync(id));
        }

        public static Task<TResponse> SendAsync<TResponse>(IRequest<TResponse> request)
        {
            return ExecuteScopeAsync(sp =>
            {
                var mediator = sp.GetService<IMediator>();

                return mediator.Send(request);
            });
        }

        public static Task SendAsync(IRequest request)
        {
            return ExecuteScopeAsync(sp =>
            {
                var mediator = sp.GetService<IMediator>();

                return mediator.Send(request);
            });
        }
    }
}
