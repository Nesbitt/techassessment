﻿namespace ToDoList.IntegrationTest.Fact
{
    using Shouldly;
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using ToDoList.Domain.Command;
    using Xunit;
    using Microsoft.EntityFrameworkCore;
    public class ToDoCreate : MockStartup
    {
        [Fact]
        public async Task Should_Create_New_ToDo()
        {
            await ClearDB<Repo.Model.ToDo>();
            // test a basic add scenario
            var toDoCreateCommand = new AddToDoItemCommand
            {
                Title = "Test title Mark",
                Summary = "This is a test title created by mark",
                StartDate = DateTime.Now.ToShortDateString(),
            };
            var commandCreatedToDo = await SendAsync(toDoCreateCommand);
            var createdToDo = await ExecuteDbContextAsync(db => db.toDo.Where(t => t.Id == commandCreatedToDo.Id).SingleOrDefaultAsync());
            createdToDo.ShouldNotBeNull();
            createdToDo.Id.ShouldBe(commandCreatedToDo.Id);
            createdToDo.Title.ShouldBe("Test title Mark");
            createdToDo.Summary.ShouldBe("This is a test title created by mark");

        }
    }
}
