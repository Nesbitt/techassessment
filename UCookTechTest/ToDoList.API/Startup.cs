﻿using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ToDoList.Domain.Command;
using ToDoList.Domain.Interface;
using ToDoList.Domain.Service;
using ToDoList.Repo.DBContext;

namespace ToDoList.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            
            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "To Do HTTP API",
                    Version = "v1",
                    Description = "Version 1 of the To Do List HTTP API",
                    TermsOfService = "ToS"
                });
            });

            services.AddMediatR();
            services.AddMediatR(typeof(GetToDoItemCommand).Assembly);
            services.AddDbContext<ToDoApplicationContext>(options => options.UseSqlServer(Configuration.GetConnectionString("ToDOListDB")));
            services.AddTransient<IToDoService, ToDoService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ToDo API v1");
            });
            app.UseMvc();
        }
    }
}
