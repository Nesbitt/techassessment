﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using ToDoList.API.Filters;
using ToDoList.Domain.Command;
using ToDoList.Domain.CommandResponse;

namespace ToDoList.API.Controllers
{
    [Produces("application/json")]
    [Route("api/v1/")]
    public class ToDoController : Controller
    {
        
        private IMediator _mediator;

        public ToDoController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("todos")]
        [HandleValidationException]
        public async Task<GetAllToDoItemsResponse> GetAllToDoItems()
        {
            var command = new GetAllToDoItemsCommand();
            return await _mediator.Send(command);
        }

        [HttpGet]
        [Route("todos/{id}")]
        [HandleValidationException]
        public async Task<GetToDoItemResponse> GetToDoItem(int id)
        {
            return await _mediator.Send(new GetToDoItemCommand { Id = id });
        }

        [HttpPost]
        [Route("todos")]
        [HandleValidationException]
        public async Task<StandardResponse> AddToDoItem(AddToDoItemCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpPut]
        [Route("todos")]
        [HandleValidationException]
        public async Task<StandardResponse> UpdateToDoItem(UpdateToDoItemCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpDelete]
        [Route("todos")]
        [HandleValidationException]
        public async Task<StandardResponse> DeleteToDoItem(DeleteToDoItemCommand command)
        {
            return await _mediator.Send(command);
        }
    }
}
